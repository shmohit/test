
<h1 align="center">
  <br>
  Gmail Mail Service Automation
  <br>
</h1>

## TOC
- [Overview](#overview)
- [Prerequisite](#prerequisite)
- [Local Deployment](#local-deployment)
- [Build & Release Pipeline](#build---release-pipeline)
    + [Key steps of the CI pipeline](#key-steps-of-the-ci-pipeline)


## Overview

Gmail Mail Service Automation automatically runs the test cases which ensure that Gmail Server is up and working as expected.
After each code check-in pipeline will run.


## Prerequisite: 

Maven, Java8

## Local Deployment

Setup: 

* Checkout the repo locally as “git@gitlab.com:shmohit/gmail-api-test-automation.git"

<br>
How to execute the automation code:-

Set email credentials (Email sender and reciver username and passwords) to the system environment variables.
On gitlab these are defined in the gitlab environment variables.

At the root directory of project, execute following command.
	"mvn clean verify"

To set up your own gmail accounts(Sender and Receiver) for testing, Follow below tutorial.
https://kb.synology.com/en-global/SRM/tutorial/How_to_use_Gmail_SMTP_server_to_send_emails_for_SRM 

## Build & release pipeline

### Key steps of the CI pipeline

* Reads the image (msharma159/automationplugin:1.0.15)
* Pre Script Execution Steps : 
    * Test execution dependencies eg. Maven, Java8 are available in the Docker image
* Test Execution Steps:
* Define the stage as test stage.
* Execute following command: 
    * Mvn clean verify
            It will run the automation test cases.<br>
* Define timeout of the pipeline so that it will be running only for 30 minutes in case of dangling state.
* Prepare artifact for ui-tactical automation at the defined path and specify when you want it to expire.


