package com.serene.tests.utilities;

import java.io.EOFException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;

import org.junit.Assert;

import com.serene.tests.constants.EmailConstants;

public class EmailApiUtility extends BasePage {

	public static String checkEmailsInInboxReceivedAfterThisTime;
	private static String senderUserName;
	private static String senderEmailpassword;
	private MimeMessage mimemessage;
	public boolean emailSuccess;
	private Properties emailSendProperties;
	private static Properties emailVerificationProperties;
	private Session session;
	private Transport transport;

	private static Folder folder = null;
	private static Store store = null;
	private static String content = "";
	private static Message messages[];

	public Session gmailLogin(String senderEmail) throws ParseException {
		checkEmailsInInboxReceivedAfterThisTime = getCurrentDateAndTime();
		
		emailSendProperties = new Properties();
		loadPropertiestoSendEmail(emailSendProperties, senderEmail);

		session = getSessionInstance(emailSendProperties, senderEmail, senderEmailpassword);
		session.setDebug(true);
		
		return session;
	}
	
	public void checkSuccessfulLogin() {
		boolean loginSuccess = session.getDebugOut().checkError();
		Assert.assertFalse("User is able to login without error !! ", loginSuccess);
	}

	public MimeMessage draftEmail(String senderEmail, String receiverEmail, String emailSubject, String emailMessage) {
		mimemessage = new MimeMessage(session);
		try {
			mimemessage.setSubject(emailSubject);
			mimemessage.setFrom(new InternetAddress(senderEmail));

			// Now set the actual message
			mimemessage.setText(emailMessage);
			mimemessage.addRecipient(Message.RecipientType.TO, new InternetAddress(receiverEmail));

		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return mimemessage;
	}

	public void checkSuccessfulEamilDraft() {
		boolean draftEmailSuccess = mimemessage.getSession().getDebugOut().checkError();
		Assert.assertFalse("User is able to login without error !! ", draftEmailSuccess);
		
	}
	public boolean sendEmail(Session session, MimeMessage mimeMessage) {
		String s = null;
		 
		try {

			// Process provides control of native processes started by ProcessBuilder.start and Runtime.exec.
			// getRuntime() returns the runtime object associated with the current Java application.
			Process p = Runtime.getRuntime().exec("ps -few");

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

			// read the output from the command
			System.out.println("Here is the standard output of the command:\n");
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}

			System.exit(0);
		} catch (IOException e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		}
		try {
			transport = session.getTransport(EmailConstants.SMTPS_EMAIL_PROTOCOL_VALUE);
			transport.connect(EmailConstants.SMTP_HOST_VALUE, Integer.valueOf(EmailConstants.SMTP_PORT_VALUE),
					senderUserName, senderEmailpassword);
			transport.sendMessage(mimemessage, mimemessage.getAllRecipients());
			transport.close();
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}

	private void loadPropertiestoSendEmail(Properties properties, String senderEmail) {
		properties.put(EmailConstants.SMTP_USER, senderEmail);
		properties.put(EmailConstants.SMTP_HOST_KEY, EmailConstants.SMTP_HOST_VALUE);
		properties.put(EmailConstants.SMTP_PORT_KEY, EmailConstants.SMTP_PORT_VALUE);
		properties.put(EmailConstants.SMTP_STARTTLS_ENABLE, "true");
		properties.put(EmailConstants.SMTP_DEBUG, "true");
		properties.put(EmailConstants.SMTP_AUTH, "true");
		properties.put(EmailConstants.SMTP_PORT_KEY, EmailConstants.SMTP_PORT_VALUE);
		properties.put(EmailConstants.SMTP_SCOKET_FACTORY_KEY, EmailConstants.SMTP_SCOKET_FACTORY_VALUE);
		properties.put(EmailConstants.SMTP_SOCKET_FACTORY_FALLBACK, "false");

	}

	private Session getSessionInstance(Properties properties, String senderEmail, String senderEmailpassword) {
		return Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(senderEmail, senderEmailpassword);
			}
		});

	}

	public void verifyMailToken(String recipientEmail, String senderEmail, String searchCriteria, String emailSubject,
			String emailMessage) throws java.text.ParseException, MessagingException {
		waitForEmailToBeReceived(10);
		Map<String, String> emailDetails = new HashMap<>();
		String password = System.getenv(EmailConstants.RECEIVER_EMAIL_PASSWORD);
		emailDetails = verifyMail(recipientEmail, password, searchCriteria, emailSubject);
		System.out.println(emailDetails);

		String actualEmailmessage = emailDetails.get("emailMessage");
		actualEmailmessage = actualEmailmessage.replace("\n", "").replace("\r", "");
		Assert.assertEquals(emailMessage, actualEmailmessage);
		Assert.assertEquals(emailSubject, emailDetails.get("emailSubject"));
		Assert.assertEquals(recipientEmail, emailDetails.get("recipient"));

	}

	public static Map<String, String> verifyMail(String userName, String password, String searchCriteria,
			String searchString) throws java.text.ParseException, MessagingException {
		Map<String, String> emailDetails = new HashMap<>();

		emailVerificationProperties = System.getProperties();
		loadPropertiesToVerifyEmail(emailVerificationProperties);

		folder = retrieveEmailFolder(userName, password, EmailConstants.INBOX_FOLDER);

		messages = retrieveEmailsFromFolder(folder);
		Assert.assertTrue("No messages found", messages.length != 0);

		// Sort messages from recent to oldest
		Arrays.sort(messages, (m1, m2) -> {
			try {
				return m2.getSentDate().compareTo(m1.getSentDate());
			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
		});

		emailDetails = retrieveLatestEmailDetails(searchString, messages);

		return emailDetails;
	}

	private static Map<String, String> retrieveLatestEmailDetails(String searchString, Message messages[])
			throws ParseException, MessagingException {
		Map<String, String> emailDetails = new HashMap<>();
		for (int i = 0; i < messages.length; i++) {
			Message msg = messages[i];
			Date currentTimeWithDate = new SimpleDateFormat(EmailConstants.DATE_TIME_FORMAT)
					.parse(checkEmailsInInboxReceivedAfterThisTime);
			try {
				if (msg.getReceivedDate().after(currentTimeWithDate)) {
					emailDetails = findAndLoadEmailDetailMap(msg, searchString);
					if (!content.equals("")) {
						break;
					}
				}
			} catch (MessagingException e) {
				e.printStackTrace();
			} finally {
				if (folder != null) {
					try {
						folder.close(true);
					} catch (MessagingException e) {
						e.printStackTrace();
					}
				}
				if (store != null) {
					try {
						store.close();
					} catch (MessagingException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return emailDetails;
	}

	private static Map<String, String> findAndLoadEmailDetailMap(Message message, String searchString) {
		Map<String, String> emailDetails = new HashMap<>();
		Object contentMessage = "";
		String strMailSubject = "", strMailBody = "";
		Object subject = null;
		try {
			subject = message.getSubject();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			contentMessage = message.getContent();
		} catch (IOException | MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		strMailSubject = (String) subject;
		if (strMailSubject.contains(searchString)) {
			content = contentMessage.toString();
			emailDetails.put(EmailConstants.EMAIL_SUBJECT, strMailSubject);
			emailDetails.put(EmailConstants.EMAIL_MESSAGE, content);
			Address[] froms = null;
			try {
				froms = message.getAllRecipients();
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			((InternetAddress) froms[0]).getAddress().toString();
			emailDetails.put("recipient", ((InternetAddress) froms[0]).getAddress().toString());
		}
		return emailDetails;

	}

	private static Message[] retrieveEmailsFromFolder(Folder folder) {
		try {
			folder.open(Folder.READ_ONLY);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Flags seen = new Flags(Flags.Flag.SEEN);
		FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
		try {
			messages = folder.search(unseenFlagTerm);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return messages;
	}

	private static Folder retrieveEmailFolder(String userName, String password, String folderName) {
		// TODO Auto-generated method stub
		Session session = Session.getDefaultInstance(emailVerificationProperties, null);
		try {
			store = session.getStore(EmailConstants.IMAP_EMAIL_PROTOCOL_VALUE);
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			store.connect(EmailConstants.IMAP_HOST_VALUE, EmailConstants.IMAP_PORT_VALUE, userName, password);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			folder = store.getFolder(folderName);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return folder;
	}

	private static void loadPropertiesToVerifyEmail(Properties properties) {
		properties.setProperty(EmailConstants.IMAP_EMAIL_PROTOCOL_KEY, EmailConstants.IMAP_EMAIL_PROTOCOL_VALUE);
		properties.setProperty(EmailConstants.IMAPS_PARTIALFETCH, "false");
		properties.put(EmailConstants.IMAP_SSL_ENABLE, "true");
		properties.put(EmailConstants.MIME_BASE64_IGNOREERRORS, "true");
		properties.put(EmailConstants.IMAP_STRTTLS_ENABLE, "true");
	}

	public void getCredentials() {
		try {
			senderUserName = System.getenv(EmailConstants.SENDER_USERNAME);
			senderEmailpassword = System.getenv(EmailConstants.SENDER_EMAIL_PASSWORD);
			Assert.assertNotNull("user name is not null", senderUserName);
			Assert.assertNotNull("Password is not null", senderEmailpassword);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	
}
